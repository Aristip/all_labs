#include <iostream>
using namespace std;

class Point {
	double x, y;
public:
	double get_x() { return x; }
	double get_y() { return y; }
	Point() { x=0; y=0; }
	Point (double xv, double yv) { x=xv; y=yv; }
	void set (double xv, double yv) { x=xv; y=yv; }
	Point & operator+ (const Point & p) {
		this -> x+=p.x;
		this -> y+=p.y;
		return *this;
	}
	friend ostream & operator<< (ostream & out, const Point & obj) {
		out << obj.x << " " << obj.y;
		return out;
	};
	friend istream & operator>> (istream & in, Point & obj) {
		in >> obj.x >> obj.y;
		return in;
	};
};