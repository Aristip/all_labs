// �10 (02.12.19).cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"
#include <fstream>
#include "Point.h"

template <class T>
void readf1(char* datp, T**& a, int& n, int& m) {
	int i, j;
	ifstream f1;
	f1 = ifstream("datp.txt");
	f1 >> n >> m;
	a = new T * [n];
	for (i = 0; i < n; i++)
		a[i] = new T[m];
	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			f1 >> a[i][j];
		}
	}
	f1.close();
}

template <class T>
void setfile(char* datp1, T** a, int n, int m) {
	int i, j;
	ofstream f2;
	f2.open("datp1.txt");
	f2 << n << " " << m << endl;
	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			f2 << a[i][j] << " ";
		}
		f2 << endl;
	}
	f2.close();
}

template <class T>
void printmatr(char* datp, T& a) {
	int i, j, n, m;
	ifstream f;
	f.open(datp);
	f >> n >> m;
	cout << n << " " << m << endl;
	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			f >> a[i][j];
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	f.close();
}

int main()
{
	int n, m; 
	Point** a;
	readf1("datp.txt", a, n, m);
	printmatr("datp.txt", a);
	setfile("datp1", a, n, m);
	system("pause");
	return 0;
}
