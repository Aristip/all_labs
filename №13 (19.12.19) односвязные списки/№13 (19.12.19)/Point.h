class Point{
	double x, y;
	char* name;
	double bal;
	int num;
public:
	
	double get_x() { return x; }
	double get_y() { return y; }
	Point() { x=0; y=0; }
	Point (double xv, double yv) { x=xv; y=yv; }
	void set (double xv, double yv) { x=xv; y=yv; }

	/*Point(char* st, double bp, int n1) {
		name = new char(strlen(st) + 1); char* p, * p1;
		p = st; p1 = name;
		while (*p != '\0') 
			*p1++ = *p++;
		*p1++ = '\0';
		num = n1;
		bal = bp;
	} */
	double get_bal() { return bal; }
	int get_num() { return num; }
	//void print() { cout << name << " " << bal << " " << num << endl; }
	Point & operator+ (const Point & p ) {
		this -> x+=p.x;
		this -> y+=p.y;
		return *this;
	}
	friend ostream & operator<< (ostream & out, const Point & obj) {
		out << obj.x << " " << obj.y;
		return out;
	};
	friend istream & operator>> (istream & in, Point & obj) {
		in >> obj.x >> obj.y;
		return in;
	};
	
};