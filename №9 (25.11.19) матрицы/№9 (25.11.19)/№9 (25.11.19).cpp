﻿// №9 (25.11.19).cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

template <class T>
class Matrixt {
	T** a;
	int n; int m;
public:
	Matrixt();
	~Matrixt() {
		for (int i = 0; i < n; i++)
			delete[] a[i];
		delete a;
	}
	Matrixt(int nl, int ml);
	Matrixt(const Matrixt&); 
	Matrixt* operator+(Matrixt& r);
	Matrixt& operator=(const Matrixt& r);///
	Matrixt* operator*(const Matrixt& r);
	Matrixt& transponirov();
	double gaus_t();
	friend ostream& operator<<(ostream& out, const Matrixt<T>& r) {
		for (int i = 0; i < r.n; i++)
			for (int j = 0; j < r.m; j++)
				out << r.a[i][j] << " ";
		out << endl;
		return out;
	}
	friend istream& operator>>(istream& in, Matrixt<T>& r) {
		for (int i = 0; i < r.n; i++)
			for (int j = 0; j < r.m; j++)
				in >> r.a[i][j];
		return in;
	}
};

template<class T>
Matrixt<T>::Matrixt() {
	n = 1; m = 1;
	a = new T * [n];
	for (int i = 0; i < n; i++)
		a[i] = new T[m];
}

template<class T>
Matrixt<T>::Matrixt(int nl, int ml) {
	n = nl; m = ml;
	a = new T * [n];
	for (int i = 0; i < n; i++)
		a[i] = new T[m];
}

template<class T>
Matrixt<T>::Matrixt(const Matrixt& r) {
	int i, j; n = r.n; m = r.n; a = new T * [n];
	for (i = 0; i < n; i++) a[i] = new T * [m];
	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++) a[i][j] = r.a[i][j];
}

template<class T>
Matrixt<T>* Matrixt<T>::operator+(Matrixt <T>& r) {
	Matrixt* z;
	z = new Matrixt(n, m);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			z->a[i][j] = a[i][j] + r.a[i][j];
	return z;
}

template <class T>
Matrixt<T>& Matrixt<T>::operator=(const Matrixt <T>& r) {
	if (&r != this) {
		if (n != r.n || m != r.m) {
			Matrixt::~Matrixt();
			n = r.n; m = r.m; a = new T * [n];
			for (int i = 0; i < n; i++)
				a[i] = new T[m];
		}
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				a[i][j] = r.a[i][j];
		return *this;
	}
}

template <class T>
Matrixt<T>& Matrixt<T>::transponirov() {
	int i, j;
	Matrixt* fp = new Matrixt(m, n);
	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++)
			fp->a[j][i] = a[i][j];
	return *fp;
}
template <class T>
Matrixt<T>* Matrixt<T>::operator*(const Matrixt <T>& r) {
	Matrixt* z; int k;
	z = new Matrixt<T>(n, r.m);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < r.m; j++)
			for (k = 0, z->a[i][j] = 0; k < m; k++)
				z->a[i][j] += a[i][k] * r.a[k][j];
	return z;
}

template <class T>
double Matrixt<T>::gaus_t() {
	int i, j, k, l, jmax, flag;
	double det, buf, t;
	for (k = 0; k < n - 1; k++)
		for (i = k + 1; i < n; i++)
			if (a[k][k] != 0) {
				buf = a[i][k] / a[k][k];
				for (j = k; j < n; j++)
					a[i][j] = a[i][j] - a[k][j] * buf;
			}
			else {
				cout << " net resh " << endl;
				return 0;
			}
	for (i = 0, det = 1; i < n; i++)
		det *= a[i][i];
	return det;
}

class Point {
	double x, y;
public:
	double get_x() { return x; }
	double get_y() { return y; }
	Point() { x = 0; y = 0; }
	Point(double xv, double yv) { x = xv; y = yv; }
	void set(double xv, double yv) { x = xv; y = yv; }
	Point& operator+(const Point& p) {
		this->x += p.x;
		this->y += p.y;
		return *this;
	}
	friend ostream& operator<<(ostream& out, const Point& obj) {
		out << obj.x << "   " << obj.y;
		return out;
	};
	friend istream& operator<<(istream& in, Point& obj) {
		in >> obj.x >> obj.y;
		return in;
	};
};

int main()
{
	Matrixt <int> at(2, 2);
	Matrixt <int> xt(1, 2);
	cout << "input at = " << endl; cin >> at;
	cout << "input xt = " << endl; cin >> xt;
	Matrixt <int>* ct; ct = new Matrixt <int>();
	ct = xt * at; cout << " ct=xt*at " << *ct << endl;
	Matrixt <int> pt(4, 4); Matrixt <int>* pt1;
	pt = xt.transponirov();
	pt1 = (*ct) * pt; cout << " pt1  " << *pt1 << endl;

/*
Matrixt <Point> bt(2,1);
Matrixt <Point> *bt1 = new Matrixt <Point> (2,1);
cin >> bt; cout << bt << endl;

Matrixt <Point> bt3(2,1); bt3 = bt;
cout << "bt3 " << bt3 << endl;
bt1 = bt + bt3;
cout << " sum " << *bt1;

Matrixt <double> zt(1,1);
cin >> zt; cout << endl << zt << endl;
*/
	system("pause");

	return 0;
}
