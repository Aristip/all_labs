// �11 (09.12.19).cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <fstream>
#include "Point.h"

template < class T >
void wwod_f(char* st, T x) {
    int size;
    fstream f;
    size = sizeof(x);
    f.open(st, ios::binary | ios::in);
    while (!f.eof()) {
        f.read((char*)&x, size);
        if (f.eof()) break;
        cout << x << endl;
    }
    f.close();
}

template < class T >
void file_bin_iz_text(char* st, char* stn, T p) {
    int len, i, j, m, n;
    fstream f;
    ifstream f1;
    f1 = ifstream(st);
    f.open(stn, ios::binary | ios::out);
    f1 >> n >> m;
    len = sizeof(T);
    for (i = 0; i < n; i++)
        for (j = 0; j < m; j++) {
            f1 >> p;
            f.write((char*)&p, len);
        }
    f1.close();
    f.close();
}

template < class T >
void file_bin_new(char* st, char* stn, T p) {
    fstream f, fn;
    int len, i, m, size;
    f.open(st, ios::binary | ios::in);
    fn.open(stn, ios::binary | ios::out);
    f.seekg(0, ios::end);
    len = f.tellg();
    size = sizeof(T);
    m = len / size;
    f.seekg(0, ios::beg);
    for (i = 0; i < m; i++) {
        f.read((char*)&p, sizeof(p));
        if (i % 2) fn.write((char*)&p, size);
    }
    f.close();
    fn.close();
}

template < class T >
void dobaw(char* st, T x) {
    fstream f;
    f.open(st, ios::binary | ios::out | ios::app);
    f.seekg(0, ios::end);
    f.write((char*)&x, sizeof(x));
    f.close();
}

int main() {
	Point p;
    file_bin_iz_text("datp.txt", "fileb_p0.dat", p);
	wwod_f("fileb_p0.dat", p);
	cout << endl;
	file_bin_new("fileb_p0.dat", "fileb_p2.dat", p);
	wwod_f("fileb_p2.dat", p);
	cout << endl;
    p = Point(10, 20);
    dobaw("fileb_p0.dat", p);
	wwod_f("fileb_p0.dat", p);
	cout << endl;
	system("pause");
    return 0;
}


